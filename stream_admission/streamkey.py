import hmac
from hashlib import sha1

from django.conf import settings


def generate(secret):
    return hmac.new(secret, settings.SECRET_KEY.encode(), sha1).hexdigest()

def check(key, secret):
    hash = hmac.new(secret.encode(), settings.SECRET_KEY.encode(), sha1).hexdigest()
    return hash == key
