from django.urls import path
from stream_admission.views import Authenticator


urlpatterns = [
    path('', Authenticator.as_view())
]