from rest_framework import serializers


class OMEClientSerializer(serializers.Serializer):
    address = serializers.IPAddressField()
    port = serializers.IntegerField()

class OMERequestSerializer(serializers.Serializer):
    direction = serializers.CharField(max_length=8)
    protocol = serializers.CharField(max_length=6)
    url = serializers.CharField(max_length=200)
    time = serializers.CharField(max_length=30)

class OMEAdmissionSerializer(serializers.Serializer):
    client = OMEClientSerializer()
    request = OMERequestSerializer()
