# Generated by Django 4.0.1 on 2022-01-07 14:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stream_admission', '0006_streamer_live'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='streamer',
            name='live',
        ),
    ]
