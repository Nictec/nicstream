from datetime import datetime

from django.contrib.auth.hashers import check_password
from rest_framework.views import APIView
from rest_framework.response import Response
from stream_admission import signature
from stream_admission.serializers import OMEAdmissionSerializer
from stream_admission.models import Streamer
from urllib.parse import urlparse
from stream_admission.url_helpers import parse_parameters
from stream_admission.streamkey import check


class Authenticator(APIView):
    def post(self, request):
        authentic = signature.check(request.body, request.headers["X-OME-Signature"])

        if not authentic:
            return Response({"allowed": False, "reason": "signature did not match"})

        serializer = OMEAdmissionSerializer(data=request.data)
        if serializer.is_valid():
            parsed_url = urlparse(serializer.data["request"]["url"])
            incoming_stream_key = parsed_url.path.split("/")[-1]
            key_list = incoming_stream_key.split(":")
        else:
            return Response(status=400)

        try:
            streamer = Streamer.objects.get(user__username=key_list[0])
        except Streamer.DoesNotExist:
            return Response({"allowed": False, "reason": f"No streamer found for key {incoming_stream_key}"})

        if not streamer.user.is_active:
            return Response({"allowed": False, "reason": "streamer is deactivated"})

        streamer.last_started = datetime.now()
        streamer.save()

        if check(key_list[1], streamer.stream_secret):
            return Response({"allowed": True})
        else:
            return Response({"allowed": False, "reason": "Stream password is false"})
