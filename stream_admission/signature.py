import base64
from hashlib import sha1
from django.conf import settings
import hmac


def check(request_data: str, signature: str) -> bool:
    secret_key = settings.OME_SECRET_KEY.encode()
    dig = hmac.new(secret_key, request_data, sha1).digest()
    hashed = base64.urlsafe_b64encode(dig).decode()[:-1]

    print(hashed, signature)
    return hashed == signature
