from typing import Dict

def parse_parameters(parameters: str) -> Dict[str, str]:
    parameter_list = parameters.split("&")

    parsed = {}
    for parameter in parameter_list:
        pair = parameter.split("=")
        parsed[pair[0]] = pair[1]

    return parsed
