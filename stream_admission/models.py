import uuid

from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password

from stream_admission import streamkey


class Streamer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    stream_secret = models.CharField(max_length=100)
    last_started = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f"Streamer: {self.user.username}"

    @property
    def stream_key(self):
        return streamkey.generate(self.stream_secret.encode())
