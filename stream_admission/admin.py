from django.contrib import admin

from player.forms import StreamerForm
from stream_admission.models import Streamer

admin.site.site_header = "NicStream Administration"
admin.site.site_title = "NicStream Administration"
admin.site.index_title = "NicStream Administration"


class StreamerAdmin(admin.ModelAdmin):
    form = StreamerForm


admin.site.register(Streamer, StreamerAdmin)
