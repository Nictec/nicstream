from django.shortcuts import get_object_or_404, render
from rest_framework.views import APIView
from rest_framework.response import Response
from stream_admission.models import  Streamer
from player.serializers import PlayerConfig, Source
from django.conf import settings

class Player(APIView):
    def get(self, request, streamer):
        streamer_data = get_object_or_404(Streamer, user__username=streamer)
        conf = PlayerConfig(data={
            "auto_start": True,
            "auto_fallback": True,
            "mute": False,
            "sources": [
                {
                    "label": f"{streamer} on NicStream",
                    "type": "webrtc",
                    "file": f"{settings.CDN_BASE_URL}{streamer_data.user.username}:{streamer_data.stream_key}"
                }
            ]
        })

        if conf.is_valid():
            return Response(conf.data)
        else:
            return Response(conf.errors)
