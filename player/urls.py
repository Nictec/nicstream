from django.urls import path
from player.views import Player

urlpatterns = [
    path('<str:streamer>/', Player.as_view()),
]
