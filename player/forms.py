from django.forms import ModelForm, PasswordInput, CharField

from stream_admission.models import Streamer


class StreamerForm(ModelForm):
    stream_secret = CharField(widget=PasswordInput())
    class Meta:
        model = Streamer
        fields = "__all__"