from typing import List

from rest_framework import serializers

class Source(serializers.Serializer):
    label = serializers.CharField(max_length=50)
    type = serializers.CharField(max_length=50)
    file = serializers.CharField(max_length=100)

class PlayerConfig(serializers.Serializer):
    auto_start = serializers.BooleanField()
    auto_fallback = serializers.BooleanField()
    mute = serializers.BooleanField(default=False)
    sources = serializers.ListField(child=Source())
