function init_player(streamer){
    fetch("/player/" + streamer)
        .then(response => response.json())
        .then(data => {
		data.waterMark = {
                	image: "https://x-net.at/de/assets/bilder/x-net-logo-grau/@@images/0910c3e2-2198-40a9-ba3e-86c47adea7be.png",
		        position: "top-left",
			y: "20px",
			x: "20px",
			width: "40px",
			height: "30px",
			opacity: 0.7,
		}
            const player = OvenPlayer.create("check-player", data)
        })
}
