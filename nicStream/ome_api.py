from django.conf import settings
import requests
from base64 import b64encode


class OMEApi:
    def __init__(self):
        self.headers = {"authorization": f"Basic {b64encode(settings.OME_API_KEY)}"}

    def _app_command(self, vhost, app, command):
        r = requests.post(f"{settings.OME_API_BASE_URL}/vhosts/{vhost}/apps/{app}:{command}", headers=self.headers)
        return r.json()

    def get_stats(self, vhost, app=None, stream=None):
        if app is None and stream is None:
            r = requests.get(f"{settings.OME_API_BASE_URL}/stats/current/{vhost}/", headers=self.headers)
        elif stream is None:
            r = requests.get(f"{settings.OME_API_BASE_URL}/stats/current/{vhost}/apps/{app}", headers=self.headers)
        else:
            r = requests.get(f"{settings.OME_API_BASE_URL}/stats/current/{vhost}/apps/{app}/streams/{stream}", headers=self.headers)

        return r.json()

    def start_recording(self, vhost, app):
        return self._app_command(vhost, app, "startRecord")

    def stop_recording(self, vhost, app):
        return self._app_command(vhost, app, "stopRecord")

    def start_push(self, vhost, app):
        return self._app_command(vhost, app, "startPush")

    def stop_push(self, vhost, app):
        return self._app_command(vhost, app, "stopPush")

